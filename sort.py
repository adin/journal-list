#!/usr/bin/env python3

from ruamel.yaml import YAML
import argparse
import re

import os

yaml = YAML()
yaml.preserve_quotes=True
yaml.width=99999 # set a stupidly high width value 

# Instantiate the parser
parser = argparse.ArgumentParser(description='Conversion tool from yaml to bibstrings')

# Required positional argument
parser.add_argument('files', type=str, nargs='+', help='List of files to process: file1.yaml file2.yml file3.yaml')
# Options
parser.add_argument('-r', '--replace', type=bool, help="Wether to replace the original file.", default=False)

args = parser.parse_args()
files = args.files
replace = args.replace


for file in files:
  sort_file = re.sub(r'\.yaml','.sort.yaml', file)
  with open(sort_file, 'w') as sort:
    with open(file, 'r') as stream:
      try:
        data = yaml.load(stream)
        data.sort(key=lambda k:k['key'])
        yaml.dump(data, sort, transform=lambda s: re.sub(r'\n+- ', '\n\n- ', s))
      except yaml.YAMLError as exc:
        print(exc)
  if replace:
    os.rename(sort_file, file)